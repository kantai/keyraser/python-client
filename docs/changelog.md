# Changelog

## 0.5: Initial release of the package.

  - Provides a client wrapping the keyraser command line client binaries
  - Support for encrypting/decrypting streams of the following formats
    - New-line delimited json (ndjson)
    - Csv file
    - Whole files with a single entity
    - Reading from/Writing to python streams
    - Reading from/Writing to files
  - Support for encrypting/decrypting single blocks
  - Create keys
  - Delete keys
